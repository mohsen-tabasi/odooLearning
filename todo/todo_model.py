# -*- coding: utf-8 -*-
"""
Models describe business objects, such as an opportunity, sales order, or partner (customer,supplier, and so on).
A model has a list of attributes and can also define its specific business.
Models are implemented using a Python class derived from an Odoo template class.
They translate directly to database objects, and Odoo automatically takes care of this when installing or upgrading the module.
"""
from odoo import models, fields

class TodoTask (models.Model):
    _name = 'todo.task';
    _description = "TO-DO Task";

    name = fields.Char("Description", required=True);
    is_done = fields.Boolean("Done?");
    active = fields.Boolean("Active?", default=True);