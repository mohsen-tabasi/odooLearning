{
    'name' : 'TO-DO Application',
    'description' : 'Manage Your Personal To-Dos',
    'author' : 'm3n',
    'depends': ['base'],
    'application' : True,
    'website' : 'www.mohsentabasi.ir',
    'license' : 'LGPL-3',
    'version' : '1.0',
    'data' : ['views/todo_menu.xml', 'views/todo_view.xml'],
}