#dietFacts Application
from odoo import models, fields;

class dietFacts_productTemplate(models.Model):
    #
    _name = 'product.template';
    _inherit = 'product.template';
    #
    calories = fields.Integer("Calories");
